// changes.js

const _ = require('lodash');
const crypto = require('crypto');
const debug = require('debug');
const fs = require('fs');
const git = require('git-client');
const path = require('path');
const Promise = require('bluebird');

const dlog = debug('gitsync:changes');

const { execCommand } = require('./exec');

function sha1sum(s)
{
    const hash = crypto.createHash('sha1');
    hash.update(s);
    return hash.digest('hex');
}

async function gitHeadCommit()
{
    const HEAD = await git.revParse({ verify: true }, 'HEAD');
    dlog({HEAD});
    return {HEAD};
}

async function gitCurrentBranch()
{
    const branch = await git.revParse({'abbrev-ref': true}, 'HEAD')
    dlog({branch});
    return {branch};
}

async function gitRoot()
{
    const root = await git.revParse({'show-toplevel': true});
    dlog({root});
    return root;
}

async function getModifiedFiles()
{
  const root = await gitRoot();
  const cmd = 'git status --porcelain';
  return Promise.resolve()
    .then(() => execCommand(cmd))
    .then(stdout => {
        const modified = [];
        const lines = _(stdout).split('\n').map(l => l.trimEnd()).compact().value();
        _.forEach(lines, async line => {
            dlog({line});
            const filePath = line.slice(3);
            dlog({filePath});
            modified.push(filePath);
        });
        return _(modified).sortBy().sortedUniq().value();
    })
    .map(file => path.join(root, file));
}

async function gitStatus(result)
{
  const cmd = 'git status --porcelain';
  const modified = {};
  const staged = {};
  return execCommand(cmd)
    .then(stdout => {
        const lines = _(stdout).split('\n').map(l => l.trimEnd()).compact().value();
        _.forEach(lines, async line => {
            dlog({line});
            const indexStatus = line[0];
            const workTreeStatus = line[1];
            const filePath = line.slice(3);
            dlog({indexStatus, workTreeStatus, filePath});
            if (workTreeStatus == 'M')
            {
                const contents = fs.readFileSync(filePath, {encoding: 'utf8'});
                const checksum = sha1sum(contents);
                const entry = {[filePath]: checksum};
                _.assign(modified, entry);
                dlog({modified});
            }
            if (indexStatus == 'M')
            {
                const cmd = `git show :${filePath}`;
                dlog({cmd});
                const contents = await execCommand(cmd);
                const checksum = sha1sum(contents);
                const entry = {[filePath]: checksum};
                _.assign(staged, entry);
                dlog({staged});
            }
        });
    })
    .then(() => {
        const statusResult = {staged, modified};
        dlog({statusResult});
        return statusResult;
    });
}

async function gitChanges()
{
    var changes = {};
    _.assign(changes, await gitHeadCommit());
    _.assign(changes, await gitCurrentBranch());
    _.assign(changes, await gitStatus());
    dlog('gitChanges:', changes);
    return changes;
}

module.exports = {
    gitChanges,
    gitCurrentBranch,
    gitHeadCommit,
    getModifiedFiles,
    gitRoot,
    gitStatus,
}

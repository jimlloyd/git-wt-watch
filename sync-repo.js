#!/usr/bin/env node

'use strict';

const _ = require('lodash');
const chai = require('chai');
const changes = require('./changes');
const config = require('./config');
const debug = require('debug');
const exec = require('./exec');
const path = require('path');
const Promise = require('bluebird');

const watchman = require('./watchman');

const { execCommand } = exec;
const { expect } = chai;
const { getConfig } = config;

const dlog = debug('gitsync:sync-repo');

// `watch` is obtained from `resp.watch` in the `watch-project` response.
// `relative_path` is obtained from `resp.relative_path` in the
// `watch-project` response.
async function make_subscription({watch, relative_path, subscriptionName, root, rootRelative, remote}) {

  const sub = {
    fields: ["name"],
  };
  if (relative_path) {
    sub.relative_root = relative_path;
  }

  const subscription = await watchman.command(['subscribe', watch, subscriptionName, sub]);
  dlog({subscription});

  // Subscription results are emitted via the subscription event.
  // Note that this emits for all subscriptions.  If you have
  // subscriptions with different `fields` you will need to check
  // the subscription name and handle the differing data accordingly.
  // `resp`  looks like this in practice:
  //
  // { root: '/private/tmp/foo',
  //   subscription: 'mysubscription',
  //   files: [ { name: 'node_modules/fb-watchman/index.js',
  //       size: 4768,
  //       exists: true,
  //       type: 'f' } ] }
  watchman.client.on('subscription', async resp =>
  {
    if (resp.subscription !== subscriptionName)
      return;

    dlog('Number of changed files:', resp.files.length);

    // This app really just wants to know when (almost) anything changes.
    // The details are irrelevant, because we will just rely on rsync to do the right thing.
    // TODO: get destination hostname from config
    // TODO: figure out the right way to specify which files rsync should ignore, and then
    // also ignore them in the subscription.
    // Note: under normal circumstances we ignore the .git diectory, but in this case
    // we absolutely want to include it. Syncrhonizing the .git directory is almost the whole point!
    const cmd = `rsync -a --delete --verbose --exclude=builds/ --exclude=node_modules/ ~/${rootRelative}/ ${remote}:${rootRelative}`;
    dlog('Running:', cmd);
    const syncresult = await execCommand(cmd);
    console.log(syncresult);
    console.log("Done!");
  });
}

async function run()
{
    const config = await getConfig('syncrepo');
    const { remote } = config;
    if (_.isNil(remote)) {
      console.error('Must specific remote server via git config syncrepo.remote');
      process.exit(1);
    }

    const root = await changes.gitRoot();
    dlog({root});

    const home = process.env.HOME + '/';
    dlog({home});

    if (!_.startsWith(root, home))
      return Promise.reject(new Error(`Inconsistent: home:${home}, root:${root}`));

    const rootRelative = root.slice(home.length);
    dlog({rootRelative});
    expect(!_.endsWith(rootRelative, '/'));

    const subscriptionName = 'sync-repo:' + path.basename(root);

    const watchResponse = await watchman.command(['watch-project', root]);
    dlog({watchResponse});

    const { warning, watch, relative_path } = watchResponse;


    if (!_.isNil(warning))
    {
        console.log('warning: ', resp.warning);
    }

    make_subscription({watch, relative_path, subscriptionName, root, rootRelative, remote});
}

return run();

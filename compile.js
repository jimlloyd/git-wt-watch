// compile.js

const _ = require('lodash');
const chalk = require('chalk');
const debug = require('debug');
const fs = require('fs');
const jsonfile = require('jsonfile');
const path = require('path');
const Promise = require('bluebird');

const { expect } = require('chai');
const { execCommand, execCommandOneLineOutput } = require('./exec');

const dlog = debug('gitsync:compile');
const vlog = debug('gitsync-verbose:compile');

const concurrency = 10;

async function loadCompileCommandsFromPath(compileCommandsPath)
{
    // A compile commands file is a JSON array of objects with keys { directory, command, file }
    // The specified file can be compiled with directory as current directory, using the specified command.
    if (!fs.existsSync(compileCommandsPath)) {
        return Promise.reject(new Error(`File ${compileCommandsPath} does not exist`));
    }
    const compileCommands = await jsonfile.readFile(compileCommandsPath);

    // A given source file may be compiled multiple times from different libraries.
    // We only want one compile command, and we (mostly) don't care which one we get.
    // So first, we make a sort list of uniq file paths
    const files = _(compileCommands).map('file').sortBy().sortedUniq().value();

    const byFilePath = _.reduce(files, (accum, file) => {
        accum[file] = _.find(compileCommands, {file});
        return accum;
    }, {});

    vlog(byFilePath);
    dlog(_.keys(byFilePath));
    return Promise.resolve(byFilePath);
}

function loadMultipleCompileCommands({builddirs})
{
    expect(builddirs).to.exist;

    const cwd = process.cwd();
    const paths = _.map(_.split(builddirs, ','), p => path.join(cwd, p, "compile_commands.json"));

    return Promise.reduce(paths, async (accum, path) =>
    {
        const commandsObj = await loadCompileCommandsFromPath(path);
        _.defaults(accum, commandsObj);
        return accum;
    }, {});

    return Promise.resolve(paths).map(compileCommandsPath => loadCompileCommandsFromPath(compileCommandsPath));
}

function transformHeaderPath(headerPath)
{
    expect(headerPath).to.be.a('string');
    const re = /^(([\./\-\w]+?)(\-(inl|fwd|hsm))?\.hpp)$/;
    const m = headerPath.match(re);
    if (m)
    {
        dlog('Match array:', m);
        const proxy = m[2] + '.cpp';
        dlog("transformHeaderPath returning proxy source path:", proxy);
        return proxy;
    }
    else
    {
        dlog("transformHeaderPath: no match for:", headerPath);
        return null;
    }
}

function transformPath(filePath)
{
    expect(filePath).to.be.a('string');
    if (_.endsWith(filePath, '.cpp')) {
        return filePath;
    } else {
        return transformHeaderPath(filePath)
    }
}

function transformMultiplePaths(filePaths)
{
    expect(filePaths).to.be.an('array');
    return _(filePaths).map(transformPath).compact().sortBy().sortedUniq().value();
}

function findCompileCommandObject({file, allCompileCommands})
{
    expect(file).to.be.a('string');
    expect(allCompileCommands).to.exist;
    if (file in allCompileCommands) {
        dlog(`findCompileCommandObject found compile command for .cpp file: ${file}`);
        return allCompileCommands[file];    // file is a .cpp file
    } else {
        const proxy = transformHeaderPath(file);
        if (proxy in allCompileCommands) {
            dlog("findCompileCommandObject using proxy source file:", proxy);
            return allCompileCommands[proxy];
        }
    }
    dlog(`findCompileCommandObject failed to find compile command for: ${file}`);
    return null;
}

async function compileChangedSourcesPaths(config, paths)
{
    const {builddirs} = config;
    dlog('compileChangedSourcesPaths:', {builddirs});
    multipleCompileCommands = await loadMultipleCompileCommands({builddirs});

    vlog({paths});

    const changedSourceFiles = _.transform(paths, (reduced, val) => {
        const file = path.join(process.cwd(), val);
        const obj = _.find(compileCommands, {file});
        if (obj)
        {
            console.log("Compiling:", file);
            reduced.push(obj);
        }
        else if (file.endsWith('.hpp'))
        {
            dlog("Skipping header file:", file);
        }
        else if (file.endsWith('.cpp'))
        {
            console.log("Unrecognized cpp file:", file);
        }
        else
        {
            dlog("No compile command for non-source file:", file);
        }
        return reduced;
    }, []);

    return Promise.map(changedSourceFiles, obj => compileOneSource(obj), {concurrency})
        .then(() => console.log("Done"));
}

async function compileChangedSourcesChanges(config, changes)
{
    const touched = _.union(_.keys(changes.staged), _.keys(changes.modified));
    return compileChangedSourcesPaths(config, touched);
}

function executedOneCompileCommand(compileCommandObj)
{
    // compileCommandObj is one object from a CMake compile_commands.json file.
    // Execute the specified compile command, modified to report just one error.
    // Also truncate the error to no more than 50 lines of text.
    const {directory, command: _command, file} = compileCommandObj;
    const command = `${_command} -fmax-errors=1`;
    return execCommand(command, {cwd: directory})
    .then(out => {
        const wilded = file.slice(0, -4) + '*';
        return execCommand(`git add ${wilded}`)
        .then(out1 => console.log(out1))
        .then(() => console.log(chalk.green(`OK: ${file}`)))
        .then(() => "")
    })
    .catch(err => {
        const errorLines = err.message.split('\n');
        const truncatedErr = errorLines.slice(0, 50).join('\n');
        console.error(chalk.bold.red(`Error: ${file}`));
        return `ERROR: ${file}\n${truncatedErr}`;
    });
}

function executeMultipleCompileCommands(compileCommands)
{
    return Promise.map(compileCommands, compileCommand => executedOneCompileCommand(compileCommand), {concurrency});
}

module.exports = {
    compileChangedSourcesChanges,
    compileChangedSourcesPaths,
    compileChangedSourcesPaths,
    executedOneCompileCommand,
    executeMultipleCompileCommands,
    findCompileCommandObject,
    loadCompileCommandsFromPath,
    loadMultipleCompileCommands,
    transformHeaderPath,
    transformPath,
    transformMultiplePaths,
};

// compile-test.js

const _ = require('lodash');
const compile = require('../compile');
const chai = require('chai');

const {expect} = chai;
const {transformHeaderPath, transformMultiplePaths} = compile;

describe('transformHeaderPath', () => {
    describe('relative path', () => {
        const expected = 'cpp/eta/array/array.cpp';
        const testPaths = [
            'cpp/eta/array/array.hpp',
            'cpp/eta/array/array-inl.hpp',
            'cpp/eta/array/array-fwd.hpp',
            'cpp/eta/array/array-hsm.hpp',
        ];

        _.forEach(testPaths, headerPath => {
            it(`${headerPath} should transform to ${expected}`, () => {
                expect(transformHeaderPath(headerPath)).to.equal(expected);
            });
        });
    });

    describe('absolute path', () => {
        const expected = '/home/jim/cpp/eta/array/array.cpp';
        const testPaths = [
            '/home/jim/cpp/eta/array/array.hpp',
            '/home/jim/cpp/eta/array/array-inl.hpp',
            '/home/jim/cpp/eta/array/array-fwd.hpp',
            '/home/jim/cpp/eta/array/array-hsm.hpp',
        ];

        _.forEach(testPaths, headerPath => {
            it(`${headerPath} should transform to ${expected}`, () => {
                expect(transformHeaderPath(headerPath)).to.equal(expected);
            });
        });
    });

    describe('negative test', () => {
        const testPaths = [
            '/home/jim/cpp/eta/array/array-xxx.hpp',
            '/home/jim/cpp/eta/array/array-aaa.hpp',
            '/home/jim/cpp/eta/array/array-b.hpp',
            '/home/jim/cpp/eta/array/array-inlx.hpp',
        ];

        _.forEach(testPaths, headerPath => {
            const expected = headerPath.replace(/.hpp$/, '.cpp');
            it(`${headerPath} should transform to ${expected}`, () => {
                expect(transformHeaderPath(headerPath)).to.equal(expected);
            });
        });
    })
});

describe('tranformMultiplePaths', () => {
    const expected = '/home/jim/cpp/eta/array/array.cpp';
    const testPaths = [
        expected,
        '/home/jim/cpp/eta/array/array.hpp',
        '/home/jim/cpp/eta/array/array-inl.hpp',
        '/home/jim/cpp/eta/array/array-fwd.hpp',
        '/home/jim/cpp/eta/array/array-hsm.hpp',
    ];

    it('Should map all variants to just one file', () => {
        expect(transformMultiplePaths(testPaths)).to.deep.equal([expected]);
    });
});

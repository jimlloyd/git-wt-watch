// compile-test.js

const _ = require('lodash');
const changes = require('../changes');
const chai = require('chai');

const {expect} = chai;
const {getModifiedFiles} = changes;

describe('getModifiedFiles hacky test', () => {
    it('is not a reproducible test, but is a convenient test harness', () => {
        return getModifiedFiles().then(files => console.log(files));
    });
});

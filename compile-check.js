#!/usr/bin/env node

'use strict';

const _ = require('lodash');
const changes = require('./changes');
const compile = require('./compile');
const config = require('./config');
const debug = require('debug');
const fs = require('fs');
const path = require('path');
const Promise = require('bluebird');

const watchman = require('./watchman');

const { expect } = require('chai');
const {
  executeMultipleCompileCommands,
  findCompileCommandObject,
  loadMultipleCompileCommands,
  transformMultiplePaths,
} = compile;

const dlog = debug('gitsync:compile-check');

// `watch` is obtained from `resp.watch` in the `watch-project` response.
// `relative_path` is obtained from `resp.relative_path` in the
// `watch-project` response.
async function make_subscription({watch, relative_path, subscriptionName, root, onChangedFiles}) {

  const clock = await watchman.clock({watch});
  dlog({clock});

  const sub = {
    expression: ["allof",
                  ["anyof", ["match", "*.cpp"], ["match", "*.hpp"]],
                  ["not", ["match", "builds/**", "wholename"]]
                ],
    fields: ["name", "exists"], // If this is reduced to just "name", the response files array changes shape.
    since: clock
  };

  if (relative_path) {
    sub.relative_root = relative_path;
  }

  const subscription = await watchman.command(['subscribe', watch, subscriptionName, sub]);
  dlog({subscription});

  // Subscription results are emitted via the subscription event.
  // Note that this emits for all subscriptions.  If you have
  // subscriptions with different `fields` you will need to check
  // the subscription name and handle the differing data accordingly.
  // `resp`  looks like this in practice:
  //
  // { root: '/private/tmp/foo',
  //   subscription: 'mysubscription',
  //   files: [ { name: 'node_modules/fb-watchman/index.js',
  //       size: 4768,
  //       exists: true,
  //       type: 'f' } ] }
  watchman.client.on('subscription', async resp =>
  {
    if (resp.subscription !== subscriptionName)
      return;

    dlog('Subscription response:', resp);
    dlog({root});
    const changed = _(resp.files).map(file => !file.exists ? null : path.join(root, file.name)).compact().value();
    await onChangedFiles(changed);
  });
}

async function run()
{
    const root = await changes.gitRoot();
    dlog({root});

    var allCompileCommands = {};
    const fullConfig = await config.getConfig('compilecheck');
    dlog(fullConfig);
    const {builddirs} = fullConfig;
    dlog({builddirs});

    if (!_.isNil(builddirs))
    {
      allCompileCommands = await loadMultipleCompileCommands({builddirs});
      const files = _.keys(allCompileCommands);
      dlog("The first compileCommandObj:", allCompileCommands[_.first(files)]);
      dlog(`Got compile commands of length ${files.length}`);
    }

    const subscriptionName = 'compile-check:' + path.basename(root);

    const watchResponse = await watchman.command(['watch-project', root]);
    dlog({watchResponse});

    const { warning, watch, relative_path } = watchResponse;

    if (!_.isNil(warning))
    {
        console.log('warning: ', resp.warning);
    }

    const onChangedFiles = async files => {
      const clearScreen = "\x1B[2J";
      const cursorHome = "\x1B[0;0H";
      const resetTerm = "\x1Bc";
      const iTerm2clearScollback = "\x1B]1337;ClearScrollback\x07"
      process.stdout.write(resetTerm);
      process.stdout.write(iTerm2clearScollback);
      process.stdout.write(clearScreen);
      process.stdout.write(cursorHome);
      console.log('Files just changed:', files);

      if (fs.existsSync('.git/index.lock'))
      {
        console.log('Git index is locked, deferring actions until unlocked');
        return Promise.resolve();
      }

      files = await changes.getModifiedFiles();
      console.log('Files git sees as modified:', files);

      const transformedPaths = transformMultiplePaths(files);
      dlog({transformedPaths});
      const compileCommandObjs = _(transformedPaths)
            .map(file => findCompileCommandObject({file, allCompileCommands})).compact().value();
      dlog(compileCommandObjs);
      return executeMultipleCompileCommands(compileCommandObjs)
        .then(results => _.each(results, result => {
          if (result) {
            console.log(result);
          }
        }))
        .then(() => console.log("Done"));
    };

    make_subscription({watch, relative_path, subscriptionName, root, onChangedFiles});
}

return run();

// watchman.js

const _ = require('lodash');
const watchman = require('fb-watchman');

const client = new watchman.Client();

function command(commandArray)
{
    return new Promise((resolve, reject) => {
        client.command(commandArray, (err, response) => {
            if (err)
                reject(err);
            else
                resolve(response);
        });
    });
}

function clock({watch})
{
  return command(['clock', watch]);
}

function getConfig({root})
{
    return command(['get-config', root]);
}

function watchProject({root})
{
    return command(['watch-project', root]);
}


module.exports = {
    client,
    command,
    clock,
    getConfig,
    watchProject,
}
